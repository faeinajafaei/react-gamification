import {
  collection,
  getDocs,
  addDoc,
  updateDoc,
  deleteDoc,
  doc,
} from "firebase/firestore";
import { db } from "../services/firebase";

const usersCollectionRef = collection(db, "studentUsers");

export const getAllStudents = async () => {
  const data = await getDocs(usersCollectionRef);
  const UserData = data.docs.map((item) => ({ ...item.data(), id: item.id }));
  return UserData;
};

export const createStudent = async (data) => {
  return await addDoc(usersCollectionRef, data);
};

export const updateStudent = async (id, totalScore) => {
  const studentDoc = doc(db, "studentUsers", id);
  const newFiled = {
    totalScore: totalScore + 1,
  };
  await updateDoc(studentDoc, newFiled);
};

export const deleteStudent = async (id) => {
  const studentDoc = doc(db, "studentUsers", id);
  await deleteDoc(studentDoc);
};
