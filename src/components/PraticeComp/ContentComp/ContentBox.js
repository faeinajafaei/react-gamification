import React from "react";
import ScorePanel from "../ScorePanel/ScorePanel";
import speaker from "../../../assets/images/others/speaker.png";
import s from "./style.module.css";
const ContentBox = ({ ContentArray, contentID, handleGetAnswer }) => {
  // const ansArray = [];
  const getAllAns = (question, ans) => {
    const ansObj = {
      question_id: question,
      answer_id: ans,
    };
    handleGetAnswer(ansObj);
    // if (ansArray.indexOf(ansObj) === -1) {
    //   ansArray.push(ansObj);
    // }
    // console.log("testValue2.2", ansArray);
  };
  const renderContent = () => {
    return (
      <>
        {ContentArray.filter((item) => item.id === contentID).map(
          (item, index) => {
            if (!item.question_label) {
              return (
                <div className={s.ContentItems} key={index}>
                  {!!item.img && (
                    <img
                      className={s.CharacterWrapper}
                      src={item.img}
                      alt="character"
                    />
                  )}
                  <span>{item.label}</span>
                  <div className={s.speakerWrapper}>
                    <img src={speaker} alt="speaker" />
                  </div>
                </div>
              );
            } else {
              return (
                <>
                  <div key={index}>
                    {!!item.img && (
                      <img
                        className={s.CharacterWrapper}
                        src={item.img}
                        alt="character"
                      />
                    )}
                    <span>{item.question_label}</span>
                  </div>
                  <div className={s.ButtonWrapper}>
                    {item.choices.map((item2, idx) => (
                      <button
                        onClick={() => getAllAns(item.question_id, item2.id)}
                        className={s.choiceButton}
                        key={idx}
                      >
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-around",
                            alignItems: "center",
                          }}
                        >
                          <span>{item2.choice_label}</span>
                          <img
                            src={speaker}
                            style={{
                              width: "70px",
                              height: "70px",
                              marginTop: 3,
                            }}
                            alt="speaker"
                          />
                        </div>
                      </button>
                    ))}
                  </div>
                </>
              );
            }
          }
        )}
      </>
    );
  };
  if (contentID <= ContentArray.length) {
    return (
      <div className={s.ContentBox}>
        <div className={s.ContentWrapper}>{renderContent()}</div>
      </div>
    );
  }
  return <ScorePanel />;
};

export default ContentBox;
