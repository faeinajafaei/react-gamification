import React from "react";
import s from "./style.module.css";
const ScorePanel = () => {
  return (
    <div className={s.scoreContainer}>
      <div className={s.scoreHeader}>
        <p>ผ่านด่าน</p>
      </div>
      <div className={s.scorebody}>
        <div className={s.levelWrapper}>
          <span>ด่านที่ 1</span>
        </div>
        <div className={s.starCount}>
          <span style={{ fontSize: "70px" }} className="material-icons-round">
            star
          </span>
          <span style={{ fontSize: "110px" }} className="material-icons-round">
            star
          </span>
          <span style={{ fontSize: "70px" }} className="material-icons-round">
            star
          </span>
        </div>
        <div className={s.scoreCount}>
          <span>คะแนน 10000</span>
        </div>
      </div>
      <div className={s.scoreFooter}>
        <button className={s.buttonStyle}>
          <span style={{ fontSize: "60px" }} className="material-icons-round">
            play_arrow
          </span>
        </button>
        <button className={s.buttonStyle}>
          <span style={{ fontSize: "60px" }} className="material-icons-round">
            refresh
          </span>
        </button>
      </div>
    </div>
  );
};

export default ScorePanel;
