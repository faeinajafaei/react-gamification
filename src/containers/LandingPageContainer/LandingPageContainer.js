import React from "react";
import { Link } from "react-router-dom";
import s from "./style.module.css";
const LandingPageContainer = () => {
  return (
    <div className={s.LandingContainer}>
      <div className={s.LandingWrapper}>
        <div className={s.HeadingPrimaryMain}>Gamification</div>
        <div className={s.HeadingPrimarySub}>Hello Im Landing page</div>
        <div className={s.btnSection}>
          {/* <Link style={{ textDecoration: "none" }} to="/login/teacher">
            <div className={`${s.btn} ${s.btnWhite} ${s.btnAnimated}`}>
              Sign in as a Teacher
            </div>
          </Link> */}
          <div className={`${s.btn} ${s.btnWhite} ${s.btnAnimated}`}>
            Sign in as a Teacher
          </div>
          <span style={{ padding: "15px" }}>or</span>
          <Link style={{ textDecoration: "none" }} to="/login/student">
            <div className={`${s.btn} ${s.btnWhite} ${s.btnAnimated}`}>
              Sign in as a student
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default LandingPageContainer;
