import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form, Button } from "react-bootstrap";
import {
  getAllStudents,
  createStudent,
  updateStudent,
  deleteStudent,
} from "../../api/FakeApi";
import {
  getUser,
  addUser,
  updateUser,
  deleteUser,
} from "../../redux/reducers/CRUDreducer";

const Crudtest = () => {
  const userData = useSelector((state) => state.crudUser);
  const dispatch = useDispatch();
  const [studentData, setStudentData] = useState({
    name: "",
    class: 0,
    totalScore: 0,
  });
  useEffect(() => {
    getAllStudents().then((res) => {
      dispatch(getUser(res));
    });
  }, [dispatch]);

  const handleCreateUser = (e) => {
    e.preventDefault();
    createStudent(studentData).then((res) => {
      dispatch(addUser({ ...studentData, id: res.id }));
    });
  };
  const handleUpdateUser = (e, value) => {
    e.preventDefault();
    updateStudent(value.id, value.totalScore);
    dispatch(updateUser(value));
  };
  const handleDeleteUser = (id) => {
    deleteStudent(id);
    dispatch(deleteUser(id));
  };
  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Name</Form.Label>
          <Form.Control
            placeholder="Name..."
            onChange={(e) =>
              setStudentData({
                ...studentData,
                name: e.target.value,
              })
            }
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Class</Form.Label>
          <Form.Control
            placeholder="Class..."
            onChange={(e) =>
              setStudentData({ ...studentData, class: Number(e.target.value) })
            }
          />
        </Form.Group>
        <Button
          variant="primary"
          type="submit"
          onClick={(e) => handleCreateUser(e)}
        >
          Create user
        </Button>

        {userData.map((item, index) => {
          return (
            <div key={index}>
              <Form.Label>name:</Form.Label>
              <span> {item.name} </span>
              <Form.Label> || totalScore:</Form.Label>
              <span> {item.totalScore}</span>
              <Button
                variant="outline-warning"
                onClick={(e) => handleUpdateUser(e, item)}
              >
                InCrease totalScore
              </Button>
              <Button
                variant="outline-danger"
                onClick={() => handleDeleteUser(item.id)}
              >
                Delete User
              </Button>
              <br />
              <br />
            </div>
          );
        })}
      </Form>
    </div>
  );
};

export default Crudtest;
