import React from "react";
import { useParams } from "react-router-dom";
import LoginForm from "../../components/LoginForm/LoginForm";
import CrudTest from "./CrudTest";
import s from "./style.module.css";
const LoginContainer = () => {
  const { role } = useParams();
  return (
    <div className={s.LoginContainer}>
      <div className={s.LoginHeader}>Login as {role}</div>
      <div className={s.LoginWrapper}>
        <div className={s.LogContent}>
          <LoginForm />
        </div>
      </div>

      <div className={s.LoginWrapper2}>
        <div className={s.LogContent}>
          <CrudTest />
        </div>
      </div>
    </div>
  );
};

export default LoginContainer;
