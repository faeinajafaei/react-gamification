import React, { useState } from "react";
import ContentBox from "../../components/PraticeComp/ContentComp/ContentBox";
import doctor from "../../assets/images/characters/doctor.png";
import teacher from "../../assets/images/characters/teacher.png";
import seniorStu from "../../assets/images/characters/seniorStu.png";
import juniorStu from "../../assets/images/characters/juniorStu.png";
import publicHealthVolunteer from "../../assets/images/characters/publicHealthVolunteer.png";
import s from "./style.module.css";

const ContentArray = [
  {
    id: 1,
    label: "การเข้าถึงบริการทางการแพทย์",
  },
  {
    id: 2,
    label:
      "การบริการทางการแพทย์เป็นการจัดการบริการเพื่อส่งเสริมสุขภาพและป้องกันโรคโดยให้น้องๆได้มีส่วนร่วมในการดูแลสุขภาพตัวเองครอบครัว และ เพื่อนๆ",
  },
  {
    id: 3,
    label:
      "เมื่อน้องๆมีปัญหาด้านสุขภาพหรือสงสัยในข้อมูลข่าวสารด้านสุขภาพ น้องๆต้องเลือกแหล่งขอคำปรึกษาที่เหมาะสมกับปัญหาเพื่อจะได้คำตอบที่ถูกต้องโดยหน่วยงานหรือบุคคลต่างๆ ที่ควรขอรับคำปรึกษาได้มีดังนี้",
  },
  {
    id: 4,
    label: "อาจารย์สุขศึกษา",
    img: teacher,
  },
  {
    id: 5,
    label: "อาสาสมัครสาธารณสุข",
    img: publicHealthVolunteer,
  },
  {
    id: 6,
    label: "ช่วงช่วยน้าหมอตอบ",
  },
  {
    id: 7,
    label:
      "เมื่อน้องมีข้อสงสัยเกี่ยวกับโทษของสุราน้องๆจะขอคำปรึกษาจากใครดีครับ",
  },
  {
    id: 8,
    question_id: 1,
    question_label: "พี่ๆจาก อาสาสมัครสาธารณสุข",
    hint: 2,
    score: 5,
    img: publicHealthVolunteer,
    choices: [
      { id: 1, choice_label: "ก. ได้" },
      { id: 2, choice_label: "ข. ไม่ได้" },
    ],
  },
  {
    id: 9,
    question_id: 2,
    question_label: "คุณครูสอนสุขศึกษา?",
    hint: 1,
    score: 5,
    img: teacher,
    choices: [
      { id: 1, choice_label: "ก. ได้" },
      { id: 2, choice_label: "ข. ไม่ได้" },
    ],
  },
  {
    id: 10,
    question_id: 3,
    question_label: "เพื่อนๆ หรือรุ่นพี่",
    hint: 2,
    score: 5,
    img: seniorStu,
    choices: [
      { id: 1, choice_label: "ก. ได้" },
      { id: 2, choice_label: "ข. ไม่ได้" },
    ],
  },
];

const PracticeContainer = () => {
  const [contentID, setContentID] = useState(1);
  const [ansData, setAnsData] = useState([]);
  const handleGetAnswer = (answerObj) => {
    setAnsData(answerObj);
  };

  const handleNextContent = () => {
    if (contentID === ContentArray.length) {
      setContentID(ContentArray.length + 1);
    } else if (contentID > ContentArray.length) {
      setContentID(1);
    } else {
      setContentID(contentID + 1);
    }
  };
  const handleBackContent = () => {
    if (contentID === 1) {
      setContentID(1);
    } else {
      setContentID(contentID - 1);
    }
  };

  return (
    <div className={s.Container}>
      <div className={s.BackgroundImg}>
        <div className={s.ContentWrapper}>
          {contentID === ContentArray.length + 1 ? (
            <>
              <ContentBox
                handleGetAnswer={handleGetAnswer}
                ContentArray={ContentArray}
                contentID={contentID}
              />
              <img
                className={s.doctorWrapper}
                src={juniorStu}
                alt="character"
              />
            </>
          ) : (
            <>
              <img className={s.doctorWrapper} src={doctor} alt="character" />
              <ContentBox
                handleGetAnswer={handleGetAnswer}
                ContentArray={ContentArray}
                contentID={contentID}
              />
            </>
          )}
        </div>
        <div className={s.ButtonWrapper}>
          <button onClick={() => handleBackContent()}>ย้อนกลับ</button>
          <button onClick={() => handleNextContent()}>ถัดไป</button>
        </div>
      </div>
    </div>
  );
};

export default PracticeContainer;
