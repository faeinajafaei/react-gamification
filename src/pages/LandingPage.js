import React from "react";
import LandingPageContainer from "../containers/LandingPageContainer/LandingPageContainer";

const LandingPage = () => {
  return <LandingPageContainer />;
};

export default LandingPage;
