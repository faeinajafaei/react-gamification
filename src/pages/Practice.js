import React from "react";
import PracticeContainer from "../containers/PracticeContainer/PracticeContainer";
const Practice = () => {
  return (
    <div>
      <PracticeContainer />
    </div>
  );
};

export default Practice;
