import { createSlice, current } from "@reduxjs/toolkit";

export const CRUDreducer = createSlice({
  name: "userInfo",
  initialState: [],
  reducers: {
    getUser: (state, action) => {
      return action.payload;
    },
    addUser: (state, action) => {
      return [...state, action.payload];
    },
    updateUser: (state, action) => {
      const allUsers = current(state);
      const allUsersData = [...allUsers];
      const EditIndex = allUsers.findIndex(
        (item) => item.id === action.payload.id
      );
      allUsersData[EditIndex] = {
        name: action.payload.name,
        class: action.payload.class,
        id: action.payload.id,
        totalScore: action.payload.totalScore + 1,
      };
      return allUsersData;
    },
    deleteUser: (state, action) => {
      const allUsers = current(state);
      const leftUsers = allUsers.filter((item) => item.id !== action.payload);
      return leftUsers;
    },
  },
});
export const { getUser, addUser, updateUser, deleteUser } = CRUDreducer.actions;
export default CRUDreducer.reducer;
