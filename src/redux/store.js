import { configureStore } from "@reduxjs/toolkit";
import CRUDreducer from "./reducers/CRUDreducer";

export default configureStore({
  reducer: {
    crudUser: CRUDreducer,
  },
});
