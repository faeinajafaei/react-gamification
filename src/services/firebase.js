import { initializeApp } from "firebase/app";
import { getFirestore } from "@firebase/firestore";
// import { getAnalytics } from "firebase/analytics";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCwTELtnyvKftVtxsw9j6u1aG0ZIy5Qu94",
  authDomain: "gamification-756f3.firebaseapp.com",
  projectId: "gamification-756f3",
  storageBucket: "gamification-756f3.appspot.com",
  messagingSenderId: "697273355142",
  appId: "1:697273355142:web:48e5a817ae49fe7b89fa07",
  measurementId: "G-88BGFSHZEP",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);

